#!/bin/bash
  

echo "Installing Docker on Ubuntu Machine"
echo "Adding key"

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "adding docker in Repo list file "

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

echo "updating apt package "

sudo apt-get update

echo " Make sure you are about to install from the Docker repo instead of the default Ubuntu 16.04 repo "

apt-cache policy docker-ce

echo " Installing Docker community Edition "

sudo apt-get install -y docker-ce

echo "if getting error like E: Could not get lock /var/lib/dpkg/lock - open (11 Resource temporarily unavailable)"
sudo dpkg --configure -a
sudo apt install -f"

echo "To give access permission of Docker to default user "
sudo usermod -aG docker ${USER}

sudo chmod 666 /var/run/docker.sock

echo "checking docker service status"
sudo systemctl status docker

