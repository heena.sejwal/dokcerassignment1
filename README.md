1)In this section we will setup DockerReferences

https://docs.docker.com/install/Assignments
Must Do
=========
Install docker on below OS using package manager, commit the solution as shell scriptUbuntu 16.04 & 18.04

Centos 6.x & 7.xInstall docker using official shell script of Docker
Good to Do
Install docker using tarball of tagged release, commit the solution as shell script.
Configure non root user to run docker commands without sudo.
sudo usermod -aG docker ${USER}


in case getting error like : "if getting error like E: Could not get lock /var/lib/dpkg/lock - open (11 Resource temporarily unavailable)" then
sudo chmod 666 /var/run/docker.sock



2) In this section we will learn about Docker ImagesReferences
https://docs.docker.com/engine/reference/commandline/images/Assignments
Must Do
i)Pull "alpine" image from docker registry and see if image is available in your local image list.

![Alt text](images/1.png?raw=true "dashboard Result")

ii)Pull some specific version of docker "alpine" image from docker registry.

![Alt text](images/2.png?raw=true "dashboard Result")

iii)Remove the image of alpine from your system.

![Alt text](images/3.png?raw=true "dashboard Result")

Good to Do
===========
Create account on DockerHub.

![Alt text](images/4.png?raw=true "dashboard Result")

Try to push the image of ubuntu in your own Docker Hub Account.

![Alt text](images/6.png?raw=true "dashboard Result")


![Alt text](images/7.png?raw=true "dashboard Result")


3)In this section we will learn about Docker Containers References
https://docs.docker.com/engine/reference/commandline/container/Assignments
Must Do
========
Run a docker container from local image "alpine" and run an inline command "ls -l" while running container.

docker run --entrypoint "/bin/ls" alpine -l /var

![Alt text](images/8.png?raw=true "dashboard Result")

after this when i am logging in the container using exec command then i am getting below error because ENTRYPOINT only specifies the executable to run, when the container starts.

my container has not be started its just the entry point has executed its command inside the container and exited out.
for ex: 
ubuntu@ip-172-31-27-212:~$ docker ps -a

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                         PORTS                     NAMES
30a430599713        tomcat:9            "catalina.sh run"        15 minutes ago      Up 15 minutes                  0.0.0.0:32769->8080/tcp   opstree.com
b81f24ad8531        alpine              "/bin/ls -l /var"        About an hour ago   Exited (0) About an hour ago                             hopeful_ptolemy
e65e1d31877b        alpine              "/bin/ls -l /home"       About an hour ago   Exited (0) About an hour ago                             fervent_cerf
2da2b136e77e        alpine              "/bin/ls -al /home"      About an hour ago   Exited (0) About an hour ago                             stupefied_swanson
b94c0bc5f6d9        alpine              "/bin/ls -al /root"      About an hour ago   Exited (0) About an hour ago                             kind_hofstadter
5398085e9b41        alpine              "/bin/ls -l"             2 hours ago         Created                                                  recursing_poincare
f0ee0bb9860d        alpine              "--entrypoint 'ls -l'"   2 hours ago         Created                                                  agitated_matsumoto
ac4dc25578b7        alpine              "-c 'ls -l' /bin/bash"   2 hours ago         Created                                                  sweet_newton

Error: No such container: /bin/bash
ubuntu@ip-172-31-27-212:~$ 


Try to take bash login to container created using "alpine" image. If it doesn't work figure it out why?


after doing the above practice  when i am logging in the container using exec command then i am getting below error because ENTRYPOINT only specifies the executable to run, when the container starts.
ubuntu@ip-172-31-27-212:~$ docker exec -it /bin/bash 29b84947f50c
Error: No such container: /bin/bash
ubuntu@ip-172-31-27-212:~$ 


Run another container in detached mode with name "opstree-docker".
![Alt text](images/9.png?raw=true "dashboard Result")

Good To Do
==============
Create a Web Page of Apache/Nginx which prints "Hello World".

![Alt text](images/11.png?raw=true "dashboard Result")

![Alt text](images/10.png?raw=true "dashboard Result")

i used docker insspect command  and then Check the IP of the container and try to open that in your browser.

![Alt text](images/12.png?raw=true "dashboard Result")





